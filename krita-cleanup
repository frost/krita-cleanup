#!/bin/zsh -f

setopt extendedglob

kra="$1"

if [[ -z "$kra" ]]; then
	echo "Usage: krita-cleanup <file>"
	exit 1
fi

autoload -Uz zmv

if [[ -e "${kra%.*}/" ]]; then
	echo "Work dir ${kra%.*}/ already exists! Aborting."
	exit 2
fi

unzip $kra -d ${kra%.*}/
cd ${kra%.*}/

IFS=$'\n'

internalimagename=$(cat maindoc.xml | grep '<IMAGE ' | head -n 1 | perl -pe 's/^.* name="(.*?)".*$/\1/g')

layerXMLs=($(cat maindoc.xml | grep '<layer '))
for layerXML in $layerXMLs; do
	layername=$(echo $layerXML | perl -pe 's/^.* name="(.*?)".*$/\1/g')
	filename=$(echo $layerXML | perl -pe 's/^.* filename="(.*?)".*$/\1/g')
	uuid=$(echo $layerXML | perl -pe 's/^.* uuid="{(.*?)}".*$/\1/g')
	newfilename=${${(L)layername}:gs/ /-/}
	echo "$layername | $filename => $newfilename"
	
	# make sure there's no name collisions
	if [[ $newfilename != $filename ]]; then
		{
			(( n=2 ))
			oldnewfilename=$newfilename
			# see https://unix.stackexchange.com/a/79304
			# function that runs with the glob-expanded list of files, counts them
			# ignore the OLD filename existing, this lets you clean already cleaned files
			while ()(($#)) ./$internalimagename/layers/$newfilename.*~./$internalimagename/layers/$filename.*(N); do
				echo "$newfilename already exists!"
				newfilename="${oldnewfilename}_$n"
				(( n++ ))
			done
		}
		(
			cd ./$internalimagename/layers/
			mv -v "$filename" "$newfilename"
			zmv -v "$filename.(*)" "$newfilename.\$1"
		)
		sed -i maindoc.xml -e "s/filename=\"$filename\"/filename=\"$newfilename\"/g"
	fi
	
	# remove UUIDs
	# convert the layer name into {00000000-0000-0000-0000-000000000000} format
	
	# this is awful...
	# the yes bit generates an endless sequence of \0's
	# then hexdump converts everything into hex
	# then awk adds dashes where necessary
	newuuid_base=$(echo -n "$newfilename$(yes $'.' | head -c 48 | perl -0777 -pe 's/.\n/\0/g')" | hexdump -n 16 -ve '/1 "%02x "')
	newuuid=$(echo "$newuuid_base" | awk '{print $1$2$3$4"-"$5$6"-"$7$8"-"$9$10"-"$11$12$13$14$15$16}')
	if [[ "$uuid" == "$newuuid" ]]; then
		echo "UUID looks good!"
	else
		echo "UUID replacement: {$uuid} => {$newuuid}"
		
		# make sure the UUID doesn't already exist
		while grep -q $newuuid maindoc.xml; do
			echo "** UUID {$newuuid} already exists! **"
			# another monster...
			# add 1 to the UUID
			# - strip out the spaces to make it a number
			# - only operate on the last 8 digits, since zsh overflows on the whole number
			# - add 1 to it
			# - smash it all back together
			# - add the spaces back in
			newuuid_base=$(printf '%s%08x' $(echo -n "$newuuid_base" | tr -d ' ' | head -c 24) $(( [#16](0x$(echo -n "$newuuid_base" | tr -d ' ' | tail -c 8) + 1) )) | hexdump -ve '"%.2s "')
			newuuid=$(echo "$newuuid_base" | awk '{print $1$2$3$4"-"$5$6"-"$7$8"-"$9$10"-"$11$12$13$14$15$16}')
			echo "Using {$newuuid} instead"
		done
		
		sed -i maindoc.xml -e "s/uuid=\"{$uuid}\"/uuid=\"{$newuuid}\"/g"
	fi
done

echo "Sorting XML attributes"
# !! xmllint drops the doctype! Snip the first two lines from the original file to keep it.
cat maindoc.xml | xmllint --nonet --nowarning --c14n - 2> /dev/null | XMLLINT_INDENT=$'\t' xmllint --format - | tail -n +2 > maindoc.xml.formatted
# Apparently Krita has things read-only
# for some reason this doesn't seem to trip up sed
chmod +w maindoc.xml
head -n 2 maindoc.xml > maindoc.xml.header
cat maindoc.xml.header maindoc.xml.formatted > maindoc.xml
chmod 0444 maindoc.xml
rm maindoc.xml.{header,formatted}

krafilename=${kra##*/}
zip -r $krafilename *
cp -i $krafilename ../${krafilename%.*}-cleaned.kra

cd ..

printf "Remove work folder ${kra%.*}/? [Y/n] "
read delete
if [[ "${(L)delete}" == "y" || -z "$delete" ]]; then
	rm -rfv ${kra%.*}/
else
	echo "Leaving it there."
fi
