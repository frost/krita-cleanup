If you open up a Krita document and dig around, you'll find that your layers are actually all named things like "layer15" and stuff.

This Will Not Do. So I threw together a little shell script to fix it.

It requires zsh, since it uses zmv for some file renaming.

#### What This Does To Your Files

The script does a couple of different types of cleanup:
- renames layer files to match the filenames
- changes maindoc.xml to point to the new layer files
- changes the UUID of each layer to be the first 16 bytes of the layer name¹, converted to hex
	- if there's a conflict, it adds 1 to the UUID and tries again

¹It actually uses the new layer filename, which is the layer name converted to lowercase and spaces replaced with dashes.

## Installation

Just put `krita-cleanup` somewhere in $PATH.

### Dependencies

- zsh
- perl
- zip and unzip

## Usage

```
krita-cleanup <file>
```
